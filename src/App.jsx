import React from "react";
import DataView from "./components/DataView/DataView"; // File - assumes .js extension if not provided.
import Navbar from "./components/Navbar"; // Directory - assumes index.js if none listed.
import "./App.css";

import Manhattan from "./components/images/manhattan.png"
import Brooklyn from "./components/images/brooklyn.png"
import Queens from "./components/images/queens.png"
import Bronx from "./components/images/bronx.png"
import StatenIsland from "./components/images/statenisland.png"

// We're using an es6 arrow function, but this could also be written:
// function App() {}

const App = () => {
  return (
    <div className="App">
      <Navbar />
      <h1 className="header">Earn More, Pay Less? </h1>
      <h2 className="subheader">
        Average Housing Costs and Incomes by NYC Borough
      </h2>
     <DataView borough="Staten Island" renterCost={14292} ownerCost={28752} renterIncome={37882} ownerIncome={94177} image={StatenIsland} />
     <DataView borough="Queens" renterCost={11234} ownerCost={15354} renterIncome={37882} ownerIncome={94177} image={Queens} />
     <DataView borough="Brooklyn" renterCost={12352} ownerCost={16542} renterIncome={37882} ownerIncome={94177} image={Brooklyn} />
     <DataView borough="Manhattan" renterCost={13965} ownerCost={18745} renterIncome={37882} ownerIncome={94177} image={Manhattan} />
     <DataView borough="Bronx" renterCost={19754} ownerCost={20123} renterIncome={37882} ownerIncome={94177} image={Bronx}/>
    </div>
  );
};

export default App;
