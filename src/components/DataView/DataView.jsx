import React from "react";
import "./DataView.css";


const DataView = ({renterCost, ownerCost, ownerIncome, renterIncome, borough, image}) => {
let dollarUSLocale = Intl.NumberFormat('en-US', {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 0
});

const boroughStyle = {
  background: `url(${image})`,
  backgroundSize: "cover"
};
  return (
    <div className="view">
      {/* left side of card */}
      <div className="view-column">
        <div  className="borough" style={boroughStyle}>
          <h1 className="borough-text">{borough}</h1>
        </div>
      </div>
      {/* right side of card */}
      <div className="view-column">
        <table className="data-view">
          <tr className="data-view-row">
            <th className="text">
              <h3 className="text-header">Renters</h3>
            </th>
            <th className="text">
              <h3 className="text-header">Homeowners</h3>
            </th>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{dollarUSLocale.format(renterCost)}</h2>
              <h3 className="text-label">Annual Housing Cost</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{dollarUSLocale.format(ownerCost)}</h2>
              <h3 className="text-label">Annual Housing Cost</h3>
            </td>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{dollarUSLocale.format(renterIncome)}</h2>
              <h3 className="text-label">Annual income</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{dollarUSLocale.format(ownerIncome)}</h2>
              <h3 className="text-label">Annual income</h3>
            </td>
          </tr>
          <tr className="data-view-row">
            <td className="text">
              <h2 className="text-data">{((renterCost/renterIncome)*100).toFixed(0)}%</h2>
              <h3 className="text-label">Expense ratio</h3>
            </td>
            <td className="text">
              <h2 className="text-data">{((ownerCost/ownerIncome)*100).toFixed(0)}%</h2>
              <h3 className="text-label">Expense ratio</h3>
            </td>
          </tr>

        </table>
      </div>
    </div>
  );
};

export default DataView;
